function [fval,fjac] = q1_jac(beta,X,y)

[nrow,ncol] = size(X);
fval = -ones(1,nrow)*exp(X*beta) + y'*X*beta - ones(1,nrow)*log(factorial(y));
fjac = -X'*exp(X*beta) + X'*y;

% we will use fminunc
fval = -fval;
fjac = -fjac;


end