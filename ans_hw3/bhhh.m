% INPUTS
%   (X,y)   : data
%   beta0   : initial value

% OUTPUTS
%   beta    : MLE
%   n       : number of iteration
%   J       : information matrix estimated by J

function [beta,n,J] = bhhh(beta0,X,y)

beta_old = beta0;

for i=1:1000
    if i == 1000
        disp('Iteration limit reached, break out')
    end
    J = ((y-exp(X*beta_old))*ones(1,6).*X)'*((y-exp(X*beta_old))*ones(1,6).*X);
    % 
    G = (sum((y-exp(X*beta_old))*ones(1,6).*X))';
    % G is Hessian of log(L)
    % neither J or G has been divided by N, they just cancel each other
    beta_new = beta_old+J\G;
    % we use mldivide symbol \
    % this is left divide symbol, solves J*(J\G)=G
    % equivalent to J^-1 * G
    n = i;
    if norm(beta_new - beta_old) < 1e-8
        beta = beta_new;
        break
    end
    beta_old = beta_new;
end
