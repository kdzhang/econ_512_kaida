function [fval] = std_nlls(beta,X,y)

[nrow,ncol] = size(X);
% sum = 0;
% for i=1:nrow
%     temp = X(i,:)'*exp(X(i,:)*beta);
%     temp = temp*temp';
%     sum = sum + temp;
% end
% H1 = sum/nrow;
% fval = H^-1 * var(y);
% 
% end     

temp = X.*(exp(X*beta)*ones(1,ncol));
H = (temp'*temp)/nrow;
% 
% conditional_var_y = exp(X*beta)*ones(1,6);
% sigma = (temp'*(conditional_var_y.*temp))/nrow;

temp2 = (y-exp(X*beta)).*temp;
sigma = (temp2'*temp2)/nrow;

fval = H^-1 * sigma * H^-1;

end
