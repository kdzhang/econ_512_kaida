addpath('../CEtools/')
clear;


%% q0

pd = makedist('gamma',2,1); % generate gamma distribution
n = 1000;
rng('default');
rng(1);
x = random(pd,n,1);   % generate random vector
y1 = sum(x)/n;
y2 = exp(sum(log(x))/n);
y = y1/y2;

% fun = @(theta) q0(theta,y);
% theta1 = bisect(fun,1,3);

theta1 = 1;
for it=1:1000
    [fval,fjac] = q0(theta1,y);
    theta1 = theta1 - fval/fjac;
    if norm(fval)<1e-6, break, end
end

theta2 = theta1/y1;

disp('The setted theta is (2,1)')
fprintf('estimated theta1 is %d\n',theta1)
fprintf('estimated theta2 is %d\n',theta2)

y = 1.1:0.05:3;
% you did not need to do this
theta_1 = y;
for i = 1:length(y)
    fun = @(theta) q0(theta,y(i));
    theta_1(i) = bisect(fun,0,100);
end

plot(theta_1,y);
xlabel('\theta_1')
ylabel('y')


%%
% addpath('../CEtools/')
% clear;
load('hw3.mat')

%% FMINUNC

% fun = @(beta) -q1(beta,X,y);
fun = @(beta) q1_jac(beta,X,y);
beta0 = [0;0;0;0;0;0];
[beta1,fval_1,~,output_1] = fminunc(fun,beta0);
n1 = output_1.iterations;
disp('')
disp('FMINUNC method:')
fprintf('The coefficients by FMINUNC are:\n')
disp(beta1)
fprintf('The FMINUNC method iterates for %d times\n',n1)
fprintf('The FIINUNC evaluation of likelihood is: %d',-fval_1)

%% FMINUNC WITH DERIVATIVE
options = optimoptions('fminunc','SpecifyObjectiveGradient',true);
[beta2,fval_2,~,output_2] = fminunc(fun,beta0,options);
n2 = output_2.iterations;
disp('')
disp('FMINUNC method with derivative:')
fprintf('The coefficients by FMINUNC-DERIVATIVE are:\n')
disp(beta2)
fprintf('The FMINUNC-DERIVATIVE method iterates for %d times\n',n2)
fprintf('The FIINUNC-DERIVATIVE evaluation of likelihood is: %d',-fval_2)

%% Nelder-Mead
fun3 = @(beta) q1(beta,X,y); 
% CompEcon uses maximum, contrary to matlab intrinsic function
optset('neldmead','maxit',5000);
optset('nedlmead','tol',1e-6);
[beta3,fval_3,n3] = neldmead(fun3,beta0);
disp('')
disp('Nelder-Mead method:')
fprintf('The coefficients by Nelder-Mead are:\n')
disp(beta3)
fprintf('The Nelder-Mead method iterates for %d times\n',n3)
fprintf('The FIINUNC-DERIVATIVE evaluation of likelihood is: %d\n',-q1_jac(beta3,X,y))

%%
% Notice that we need to change the default iteration maximum and tol
% in CompEcon to get the converged result.


%% BHHH
[beta4,n4,J] = bhhh(beta0,X,y);
disp('')
disp('BHHH method:')
fprintf('The coefficients by BHHH are:\n')
disp(beta4)
fprintf('The BHHH method iterates for %d times\n',n4)
fprintf('The BHHH evaluation of likelihood is: %d\n',-q1_jac(beta4,X,y))

e_eval = eig(J);
J_init = ((y-exp(X*beta0))*ones(1,6).*X)'*((y-exp(X*beta0))*ones(1,6).*X);
e_init = eig(J_init);
fprintf('\nThe eigenvalues for the estimated Hessian is:\n')
disp(e_eval')
fprintf('\nThe eigenvalues for the initial Hessian is:\n')
disp(e_init')

%%
% Notice that the Hessian at estimated value and at initial value are not
% very different.
% Thus we may just use Hessian at initial value to update beta,
% this may be more efficient.
% Also notice that both eigenvalues are positve,
% thus the function to be evaluated is locally convex.

%% NLLS 

fun2 = @(beta) sum((y-exp(X*beta)).^2);
[beta5,fval_5,~,output_5] = fminunc(fun2,beta0);
n5 = output_5.iterations;
disp('')
disp('Estimate the NLLS estimator use FMINUNC:')
fprintf('The coefficients by FMINUNC are:\n')
disp(beta5)
fprintf('The FMINUNC method iterates for %d times\n',n5)


%% Standard deviation
% 
var_mle = J^-1;
std_mle = sqrt(diag(var_mle));
disp('The variance matrix for MLE is: ')
disp(var_mle)
% 
var_nlls = std_nlls(beta5,X,y);
std_nlls = sqrt(diag(var_nlls));
disp('The variance matrix for NLLS is: ')
disp(var_nlls)