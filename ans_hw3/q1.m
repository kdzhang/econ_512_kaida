function fval = q1(beta,X,y)

[nrow,ncol] = size(X);
fval = -ones(1,nrow)*exp(X*beta) + y'*X*beta - ones(1,nrow)*log(factorial(y));

end