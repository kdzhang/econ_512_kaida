function [fval]=covariance_theta(theta1,theta2,x)

n = length(x);
temp = [0,0];
temp(1) = log(theta2)+mean(log(x))-psi_CE(theta1);
temp(2) = theta1/theta2-mean(x);
temp = temp*sqrt(n);
fval = temp'*temp;

end
