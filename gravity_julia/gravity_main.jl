##  Gravity model Estimation
# Code written by Felix Tintelnot, March 2010
# Modifications by M. Roberts, Sept 2010; Markdown by C Murry, 2015
# Juliarized by Kaida Zhang, 2017

# Outline of code
#
# * Guess a P vector (recall P is the price index for each country,
#    which is a solution of a system of non-linear equations)
# * Given P vector estimate the gravity model
# * Using trade cost estimates,
#    solve for new P vector using root-finding technique
# * Repeat using new P vector

workspace()

using MAT # to read matlab data file

println("  ");
println("Standard A&vW gravity estimation");
println("  ");

file = matopen("/users/mac/desktop/econ_512_2017/gravityExample/trdata.mat")
trdata = read(file,"trdata")
close(file)

trade   = trdata[:,4];    #Bilateral trade flows (cif values)
dis     = trdata[:,1];    #Distance
expgdp  = trdata[:,2];    #Exporter's GDP
impgdp  = trdata[:,3];    #Importer's GDP
lang    = trdata[:,5];    #dummy = 1 if both countries share an official language or >9% of population speak same language
hist    = trdata[:,6];    #dummy = 1 if both countryies share common history (same country in the past or colonial ties)
eu      = trdata[:,7];    #dummy = 1 if both countries are member EU27/EFTA
nafta   = trdata[:,8];    #dummy = 1 if both countries are member NAFTA
asean   = trdata[:,9];    #dummy = 1 if both countries are member ASEAN

struct mm
  N::Int
  y::Array{Float64}
  sigma::Float64
  tr::Array{Float64}
  di::Array{Float64}
  eg::Array{Float64}
  ig::Array{Float64}
  la::Array{Float64}
  hi::Array{Float64}
  eu::Array{Float64}
  na::Array{Float64}
  as::Array{Float64}
end
# in julia, strcy has to be claimed before using


N       = convert(Int,sqrt(size(trade,1)));    # Number of countries
y       = impgdp[1:N];     # GDP of each country
sigma = 5;


##
# Eliminate bilateral trade flows that are missing
mis     = 0;
for i=1:N*N             # Count number of missing tradeflows
  if trade[i] == 99999
    mis=mis+1;
  end
end
println("missing number of trade flows $(mis)")


i = 1;
j = 1;
id = 1:N*N;        # observation number
id = id';

nid = 1:N*N-mis;  # nid contains the obs number of all non-missing obs
while i <= N*N
  if !(trade[i] == 99999)
    nid[j] = id[i];
    j=j+1;
  end
  i=i+1;
end


##
# Eliminate all missing trade flows in the data.  These variables are
# used in the NL least squares regression

tr     = trade[nid];
di     = dis[nid];
eg     = expgdp[nid];
ig     = impgdp[nid];
la     = lang[nid];
hi     = hist[nid];
eu     = eu[nid];
na     = nafta[nid];
as     = asean[nid];


## Start computational algorithm, standard A&vW regression

##
# *Initial Guesses*

P       = ones(N,1);     # Country Price index, initial value
beta    = ones(7,1);     # Coefficients to be estimated, initial guess
Pnew    = copy(P);
Pold    = copy(P)*5;
j       = 1;

##
# *Outer iteration over the Price Indicies (while loop)*

tol = 1e-11 ;
while abs(Pnew-Pold)>tol
  P       = Pnew;
  # * Country price index for the exporting country (Pexprel)
  # * each block of N values is the price index for one exporting country
  # * Country price index of the importing country (Pimprel)
  # * each block on N values is the price index for the N importing countries
  unitv = ones(N,1) ;
  Pexprel = kron(P,unitv) ;
  Pimprel = kron(unitv,P) ;
