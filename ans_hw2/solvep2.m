function [fval,count] = solvep2(p)

e = 1;
count = 0;
a = bertrand(p);
b = bertrand_deriv(p);
% while e > 1e-15
%     if count > 10000
%         break
%     end
%     pnew = p-bertrand(p)./bertrand_deriv(p);
%     e = abs(pnew-p);
%     p = pnew;
%     count = count+1;
% end


% the below uses secant method insead of assuming derivative
while e > 1e-15
    if count > 10000
        break
    end
    pnew = p-a./b;
    pnew = [p(1);pnew(2);p(3)];
    a = bertrand(pnew);
    b = (a-bertrand(p))./(pnew-p);
    e = abs(pnew(2)-p(2));
    p(2) = pnew(2);
    count = count+1;
end


fval = p(2);

end