function [fval,count] = solvep3(p)

e = 1;
count = 0;
a = bertrand(p);
b = bertrand_deriv(p);
% while e > 1e-15
%     if count > 10000
%         break
%     end
%     pnew = p-bertrand(p)./bertrand_deriv(p);
%     e = abs(pnew-p);
%     p = pnew;
%     count = count+1;
% end


% the below uses secant method insead of assuming derivative
while e > 1e-15
    if count > 10000
        break
    end
    pnew = p-a./b;
    pnew = [p(1);p(2);pnew(3)];
    a = bertrand(pnew);
    b = (a-bertrand(p))./(pnew-p);
    e = abs(pnew(3)-p(3));
    p(3) = pnew(3);
    count = count+1;
end


fval = p(3);

end