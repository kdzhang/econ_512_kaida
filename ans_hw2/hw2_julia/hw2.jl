workspace()
cd(Base.source_dir())
include("simpletools.jl")
using SimpleTools
auto_add_package(["PyPlot","GLM","DataFrames"])
