module SimpleTools

function auto_add_package(x::Array{String})
  pkgstatus = Pkg.installed()
  for i in 1:length(x)
    if get(pkgstatus,x[i],0)==0
      println("Package $(x[i]) is going to be added.")
      println("  ")
      Pkg.add(x[i])
    end
  end
end

function auto_add_package(x::String)
  pkgstatus = Pkg.installed()
  if get(pkgstatus,x,0)==0
    println("Package $(x) is going to be added.")
    println("  ")
    Pkg.add(x[i])
  end
end


end
