function q=mktshare(p)

% both v and p need to be row vector
% some checking code for v and p needed
v=[-1;-1;-1];
denom = 1+sum(exp(v-p));
sizev = size(v);    % matlab can't index over temporary matrix, have to add this
q = 1:sizev(1);
for i=1:sizev(1)
    q(i) = exp(v(i)-p(i))/denom;
end
q = q';
end