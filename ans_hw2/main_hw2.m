addpath('../CEtools/')
clear;


%% Q1
disp(' ');
disp('Question 1: ');
disp(' ');

disp('The mkt share is: ');
q = mktshare([1;1;1]);
disp(q');

%% Q2

disp(' ');
disp('Question 2: ');
disp(' ');

pmatrix = [1,1,1;
    0,0,0;
    0,1,2;
    3,2,1];
pmatrix = pmatrix';
broyden_time = 1:4;
for i=1:4
    tic
    broyden_eqm = broyden('bertrand',pmatrix(:,i));
    % you could also extract the number of iterations it took to run like
    % [broyden_eqm,~,~,nit] = broyden(...)
    toc
    broyden_time(i)= toc;
    fprintf('\nThe eqm under initial p%d is: ',i)
    disp(broyden_eqm');
    disp(' ');
end

%% Q3

disp(' ');
disp('Question 3: ');
disp(' ');

q3_time = 1:4;
for i=1:4
    tic
    [q3_eqm,q3_count] = q3(pmatrix(:,i));
    toc
    q3_time(i)= toc;
    fprintf('\nThe eqm under initial p%d is: ',i);
    disp(q3_eqm');
    fprintf('The iteration ends in %d times\n\n', q3_count);   
    disp(' ');
end

%%
% Notice that this function is quite slow, but the precision has not
% increase. Because it is of no sense to do subiteration, since it has to
% be updated in the next step.


%% Q4

disp(' ');
disp('Question 4: ');
disp(' ');

q4_time = 1:4;
for i=1:4
    tic
    [q4_eqm,q4_count] = q4(pmatrix(:,i));
    toc
    q4_time(i)= toc;
    fprintf('\nThe eqm under initial p%d is: ',i)
    disp(q4_eqm');
    fprintf('The iteration ends in %d times\n\n', q4_count)
    disp(' ');
end

%%
% We can see this is very fast, even faster than the Broyden method.


%% Q5

disp(' ');
disp('Question 5: ');
disp(' ');

q5_time = 1:4;
for i=1:4
    tic
    [q5_eqm,q5_count] = q5(pmatrix(:,i));
    toc
    q5_time(i)= toc;
    fprintf('\nThe eqm under initial p%d is: ',i);
    disp(q5_eqm');
    fprintf('The iteration ends in %d times\n\n', q5_count);   
    disp(' ');
end


%%
% Notice that this is much faster than q3 approach.

