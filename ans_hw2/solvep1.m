function [fval,count] = solvep1(p)

e = 1;
count = 0;
a = bertrand(p);
b = bertrand_deriv(p);

% the below uses secant method insead of assuming derivative
while e > 1e-15
    if count > 10000
        break
    end
    pnew = p-a./b;
    pnew = [pnew(1);p(2);p(3)];
    a = bertrand(pnew);
    b = (a-bertrand(p))./(pnew-p);
    e = abs(pnew(1)-p(1));
    p(1) = pnew(1);
    count = count+1;
end


fval = p(1);

end