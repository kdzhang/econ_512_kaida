function fval = bertrand_deriv(p)

% v = [-1;-1;-1];
q = mktshare(p);
% fval = q.*(1-q).*(p.*(1-2*q)-2);
fval = q.*([1;1;1]-p+p.*q)-[1;1;1];


end
