function [fval,count] = q3(p)

e = 1;
count = 0;
pnew = p;


while e >1e-15
    if count > 10000
        break
    end
    [pnew(1),count1]=solvep1(p);
    [pnew(2),count2]=solvep2(p);
    [pnew(3),count3]=solvep3(p);
    e = max(abs(p-pnew));
    p = pnew;
    count = count+count1+count2+count3;
end

fval = p;
end