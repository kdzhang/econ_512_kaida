function fval = bertrand(p)

% v = [-1;-1;-1];
q = mktshare(p);
% fval = q - p.*q.*(1-q);
fval = 1+p.*q-p;

end
