# Q6
# Assume error term is independent across time and across firms

workspace()
cd(Base.source_dir())

include("simpletools.jl")
using SimpleTools
auto_add_package(["GLM","DataFrames"])

import GLM
import DataFrames

function main()
  table = DataFrames.readtable("datahw1.csv")
  DataFrames.names!(table,[:firm_id,:year,:export_dm,:rd_dm,:productivity,:capital])
  # rename the table columns
  lm1 = GLM.fit(GLM.LinearModel,DataFrames.@formula(productivity ~ export_dm + rd_dm + capital),table)
  # @time GLM.fit(GLM.LinearModel,DataFrames.@formula(productivity ~ export_dm + rd_dm + capital),table)
  print(lm1)
end

main()
