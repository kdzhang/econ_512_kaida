hw1-all-questions.jl is the julia excutable file for all questions. use command `include("path-of-this-file")`

** Below msg is for previous version. No longer useful.

empirical-methods-hw1.ipynb is the answer for question 1-5, which can be open by Jupyter Notebook.
hw1-q6.jl is the answer for question 6.
