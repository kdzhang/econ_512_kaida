workspace()
cd(Base.source_dir())
include("simpletools.jl")
using SimpleTools
auto_add_package(["PyPlot","GLM","DataFrames"])

using PyPlot
function Q1()
  println("  ");
  println("Q1");
  println("  ");

  x = [1, 1.5, 3, 4, 5, 7, 9, 10]
  y1 = -2 + 0.5x
  y2 = -2 + 0.5(x.^2)

  filepath = Base.source_dir()
  a = plot(x,y1)
  patha = joinpath(filepath,"a.png")
  savefig(patha) # save only one line

  b = plot(x,y2)
  pathb = joinpath(filepath,"b.png")
  savefig(pathb) # save two lines in one plot

  println("Two graphs have been saved in the folder.")
end

# Q1()

function Q2()
  println("  ");
  println("Q2");
  println("  ");

  x = linspace(-10,20,200)
  # x = x'
  print("sum(x) is $(sum(x))")
end
# Q2()

function Q3()
  println("  ");
  println("Q3");
  println("  ");

  A = [2 4 6;
  1 7 5;
  3 12 4]
  B = [-2;3;10]
  C = A'B
  D = inv(A'A)B
  E = sum(sum(A[i,j]*B[i] for i=1:3) for j=1:3)

  row_index = [1,3]
  column_index = [1,2]
  F = A[row_index,column_index]

  x = inv(A)B

  println("A is $A")
  println("B is $B")
  println("C is $C")
  println("D is $D")
  println("E is $E")
  println("F is $F")
  println("x is $x")
end
# Q3()

function Q4()
  println("  ");
  println("Q4");
  println("  ");

  A = [2 4 6;
  1 7 5;
  3 12 4]

  M = zeros(3,3)

  B = [A M M;
      M A M;
      M M A]

  println("B is $B")
end
# Q4()

function Q5()
  println("  ");
  println("Q5");
  println("  ");

  A = 5*randn(5,3)+10

  # SHOULD HAVE A MUCH PRETTY WAY OF CREATING B
  B = zeros(5,3)
  for i=1:5
      for j=1:3
          if A[i,j]>=10
              B[i,j]=1
          end
      end
  end

  println("A is $A")
  println("B is $B")
end
# Q5()

import GLM
import DataFrames

function Q6()
  table = DataFrames.readtable("datahw1.csv")
  DataFrames.names!(table,[:firm_id,:year,:export_dm,:rd_dm,:productivity,:capital])
  # rename the table columns
  lm1 = GLM.fit(GLM.LinearModel,DataFrames.@formula(productivity ~ export_dm + rd_dm + capital),table)
  # @time GLM.fit(GLM.LinearModel,DataFrames.@formula(productivity ~ export_dm + rd_dm + capital),table)
  print(lm1)
end
# Q6()

function main()
  Q1()
  Q2()
  Q3()
  Q4()
  Q5()
  Q6()
end

main()
